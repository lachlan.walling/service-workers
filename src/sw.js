const fallback = () => {
  return new Response('Network error', {
    status: 408,
    headers: { 'Content-Type': 'text/plain' },
  });
}

const handleRequest = async (event) => {
  console.log(`fetching ${event.request.url}`)
  return fallback()
}

addEventListener("fetch", async (event) =>
  event.respondWith(handleRequest(event))
)