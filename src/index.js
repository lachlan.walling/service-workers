const registerServiceWorker = async () => {
  if ('serviceWorker' in navigator) {
    try {
      await navigator.serviceWorker.register(
        'sw.js',
      );
    } catch (error) {
      console.error(`Registration failed with ${error}`);
    }
  }
};

window.addEventListener('load', registerServiceWorker)